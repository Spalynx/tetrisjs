import {GameBoard} from './board.js';
import {Tetronimo} from './tetronimo.js';
import * as Util from './util.js';

var gameloopIntervalId;
var frameTimeMs = 500;

/** Represents the entire Tetris "game".
 * This includes user input, the board data/canvas, scoring, and level data.
 */
export class Game {

    //A user's "hovering" block.
    // It never exists on the board until placed, but is just drawn on top of it.
    CursorX = 0;
    CursorY = 0;
    CurrentBlockId = 0;
    CurrentBlock = [];

    TouchedDown = 0;

    //When you don't want the user to move until next tick.
    LockMovement = false;
    Board = new GameBoard();

    Score = 0;
    Level = 1;

    constructor() {
        this.newCursor();
        document.addEventListener("keydown", this.keyDownHandler.bind(this), false);
        this.gameLoop();
    }


    /** Calls game ticks at an interval, handles game start/stop events.
    */
    gameLoop(){
        this.gameloopIntervalId = setInterval(this.gameTick.bind(this), frameTimeMs);
    }

    /** Represents a single game tick.
        The main flow here is placing a hovering block (if it needs to be),
        and then drawing the board.
     */
    gameTick(){
        //I cant believe vanilla html doesn't have default binding, like wtf.
        document.getElementById("Score").innerHTML = this.Score;
        document.getElementById("Level").innerHTML = this.Level;

        //The user cannot move until this is reset.
        this.LockMovement = true;

        if(this.TouchedDown >= 2){
            this.Board.placeBlockOnBoard(this.CursorX, this.CursorY, this.CurrentBlock, this.CurrentBlockId);
            this.Score += this.Board.scoreLines(this.Level);
            this.newCursor();

            if(this.Board.LinesScored >= 10){
                this.Level++;
                this.Board.LinesScored -= 10;
                if(this.frameTimeMs >= 20){
                    this.frameTimeMs -= 20;
                }
            }
            this.TouchedDown = 0;
        }
        else{
            if(!this.moveCursor(0,1)){
                this.TouchedDown++;
            }
        }
     
        this.Board.drawGameBoard(this.CursorX, this.CursorY, this.CurrentBlock, this.CurrentBlockId);

        //A new tick should always reset movement.
        this.LockMovement = false;
    }

    /** Gives the user a new block to move.
     */
    newCursor(){
        this.CursorX = Math.floor(this.Board.Columns/2) - 1;
        this.CursorY = 0;

        this.CurrentBlockId = Math.floor(Math.random() * 7) + 1;
        this.CurrentBlock = Tetronimo.setGeometry(this.CurrentBlockId);

        if(!this.isLegalMove(this.CursorX, this.CursorY)){
            this.gameOver();
        }
    }

    gameOver(){
        //Theoretically kill the gameloop interval.
        alert("YOU SUCK OH SHIIIIIII");
    }
    
    /** Moves the "hovering" cursor block in any direction.

        @param {int} deltaX The change in X value of the cursor.
        @param {int} deltaY The change in Y value of the cursor.
     */
    moveCursor(deltaX, deltaY){
        var newCursorX = this.CursorX + deltaX;
        var newCursorY = this.CursorY + deltaY;

        if(this.isLegalMove(newCursorX, newCursorY)){
            this.CursorX = newCursorX;
            this.CursorY = newCursorY;
            return true;
        }
        return false;
    }

    /** Rotates the stored current "hovering" block 90 degrees.
        Only goes clockwise because doing both ways sounds hard.
     */
    rotateCursor(){
        //create temp 
        var temp = Util.cloneMatrix(this.CurrentBlock);
        //rotate current
        Util.rotateMatrix(this.CurrentBlock); 

        //Is the rotated block legal in the current space?
        if(this.isLegalMove(this.CursorX, this.CursorY)){
            return true;
        }
        else {
            //replace current block with the cloned old block.
            this.CurrentBlock = temp;
            return false;
        }
    }
    /** Loops moving the block downwards until it hits an illegal move and ends the turn.
     */
    dropCursor(){
        while(this.moveCursor(0,1)){
            this.Score += 2;
        };

        this.TouchedDown++;
        this.LockMovement = true;
    }

    /** Checks to see if a theoretical move of the current block would cause an intersection.
     */
    isLegalMove(x, y){
        //V1 -- Just check to see if it's in bounds.
        /**
        var blockHeight = this.CurrentBlock.length;
        var blockWidth = this.CurrentBlock[0].length;

        if(y+blockHeight >= this.Board.Rows+1 || y < 0) return false;
        if(x+blockWidth >= this.Board.Columns+1 || x < 0) return false;
        return true;
        **/
        
        //V2 -- Get a sum of blocks before and after placing current cursor and check diff.
        /**
        //get copy of the board
        var oldBoard = Util.cloneMatrix(this.Board.Board);
        //place block on the board
        this.Board.placeBlockOnBoard(this.CurrentBlock, x, y);

        //get a count of all non zero numbers in the old array
        var oldBlocksSum = Util.SumMatrix(oldBoard) + Util.SumMatrix(this.CurrentBlock);
        var newBlocksSum = Util.SumMatrix(this.Board.Board);

        this.Board.Board = oldBoard;
        if(oldBlocksSum == newBlocksSum) {
            //Current move is good, now check to see if the next vert move is toucheddown
            return true;
        }
        else {
            return false;
            //BUT now you need to do edge detection on the bottom of the block
                // if you're touching on the bottom that's a condition to place the block.
            //if temp count is under old count that means you've got an intersection
            //if temp count is over old count then WHAT THE FUCK
        }
        */


        //V3 -- Actually doing things right. Place block and look for intersection, and also check
        //	an index below current "solid" block for other blocks. If they touch, you should
        //	set as "touched".


        //var transientTouchedDown = false;
        //foreach row in block 
            // foreach element in a row of the block
                // check if current block would collide with something "solid" on the board
                // if it collides, return false
                // else if we're not on the last row
                    // check if a single row below has a solid block, in this column.
                    // set transient touched down to true
                // else continue (we're on the last row, and there's no collision)

        //if transient is true set touched to true.
            //assuming we got this far, that means we have something below a solid block,
                //but we don't have collisions!

        //return true

        //Out of bounds, simply a wrong move.
        if(x > this.Board.Columns || y > this.Board.Rows) return false;
        //if(x < 0 || y < 0) return false;

        var trimmedBlock = Util.TrimEmptyRows(this.CurrentBlock);

        //Look for actual solids collisions.

        for(let [blockY, row] of trimmedBlock.entries()){
            for(let [blockX, element] of row.entries()){
                var xLoc = x + blockX;
                var yLoc = y + blockY;

                //Is it out of bounds? Fuck it!
                if(xLoc > this.Board.Columns - 1 && element != 0)
                    return false;
                //Cant go any lower
                else if(yLoc > this.Board.Rows - 1)
                    return false;
                //Is there a "solids" collision? Not a good move!
                else if(this.Board.Board[yLoc][xLoc] != 0 && this.CurrentBlock[blockY][blockX] != 0)
                    return false;
            }
        }

    	//No collisions, good! But we gotta check for a touchdown.
        var bottomIndex = trimmedBlock.length + y;
        var indexBelowLastRow = trimmedBlock.length + y; //+1 from looking below, but also -1 from length.

        if(indexBelowLastRow == this.Board.Rows){
            this.TouchedDown++;
        }
        else {
            trimmedBlock.pop().forEach((element, blockX) => {
                var xLoc = x + blockX;

                //slight inefficiency, but fuq it
                if(element != 0 && this.Board.Board[indexBelowLastRow][xLoc] != 0)
                    this.TouchedDown++;
                else
                    this.TouchedDown = 0;
            });
        }

        //All is good in the world.
        return true;

        //Hol up a minute. If I just specify that its a downwards move... any collision detection is a touchdown, right?
    }

    /** User input handler for the game.

        Only uses arrow keys and space.
     */
    keyDownHandler(event){
        if(this.LockMovement) return;

        switch(event.keyCode){
            //priority 1
            case 39:
                this.moveCursor(1,0);
                console.log("right");
                break;
            case 37:
                this.moveCursor(-1,0);
                console.log("left");
                break;
            case 40:
                this.moveCursor(0, 1);
                if(this.TouchedDown == 0) this.Score += 1;
                console.log("down");
                break;
            case 38:
                this.rotateCursor(this.CurrentBlock);
                console.log("up");
                break;
            case 32:
                this.dropCursor();
                console.log("space");
                break;
            //priority 2
            //enter/escape for pause, home row keys for extended mode
            case 13:
                console.log("enter");
                break;
            default:
                console.log(`Button not handled: ${event.keyCode}`);
                break;
        }

        var faces = Tetronimo.setFaces(this.CurrentBlockId)
        this.Board.drawGameBoard(this.CursorX, this.CursorY, this.CurrentBlock, faces);
    }
}

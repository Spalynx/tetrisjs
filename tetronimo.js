export class Tetronimo {
    constructor(block = 0){
        this.geometry = this.setGeometry(block);
        this.face = this.setFaces(block);
    }

    static setGeometry(block = 0) {
        switch(block){
            case 0: return [[0]];
            case 1: return [[0, 0, 0],
                            [1, 1, 1],
                            [0, 1, 0]];
            case 2: return [[0, 0, 0],
                            [1, 0, 0],
                            [1, 1, 1]];
            case 3: return [[0, 0, 0],
                            [1, 1, 0],
                            [0, 1, 1]];
            case 4: return [[1, 1],
                            [1, 1]];
            case 5: return [[0, 0, 0],
                            [0, 0, 1],
                            [1, 1, 1]];
            case 6: return [[0, 0, 0],
                            [0, 1, 1],
                            [1, 1, 0]];
            case 7: return [[0, 0, 0, 0],
                            [1, 1, 1, 1],
                            [0, 0, 0, 0],
                            [0, 0, 0, 0]];
            default: return this.setGeometry();
        }
    }

    //Currently going to use color or something,
    // but I'd like some sort of graphic in the future.
    static setFaces(block = 0) {
        switch(block){
            case 0: return "rgba(0, 0, 0, 0.1)";
            case 1: return '#800080';
            case 2: return '#0000FF';
            case 3: return '#FF0000';
            case 4: return '#FFFF00';
            case 5: return '#FFA500';
            case 6: return '#00FF00';
            case 7: return '#00FFFF';
            default: return this.setFaces();
        }
    }
}

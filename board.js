import {Tetronimo} from './tetronimo.js';

/** Represents the Tetris game board, including underlying data AND the front end canvas object.
 *
 *  In a sense, it would be ideal to separate this out more, but it's Tetris. The scope isn't
 *  gonna change that much.
 */
export class GameBoard {
    Rows = 20;
    Columns = 10;
    GapPx = 1; //Pixel gap between blocks.

    Board = Array.from({ length: this.Rows }, () => new Array(this.Columns).fill(0));
    Canvas = document.createElement("canvas");
    Context;

    //Canvas size/location.
    X = 0;
    Y = 0;
    Width = 250;
    Height = 500;

    LinesScored = 0;

    constructor(){
        this.Canvas.width = this.Width;
        this.Canvas.height = this.Height;
        this.Context = this.Canvas.getContext("2d");
        document.body.insertBefore(this.Canvas, document.body.childNodes[0]);
    }

    /** Wipe the entire game board from the canvas.
      */
    clearBoard() {
        let ctx = this.Context;
        ctx.clearRect(0, 0, this.Canvas.width, this.Canvas.height);
    }

    /** Writes a "floating" (user controlled) block to the board grid.
        @param {number} x Current X value of the cursor.
        @param {number} y Current Y value of the cursor.
        @param {number[][]} block A matrix representing a user block.
        @param {number} blockId A value designating which type of tetronimo this is.
      */
    placeBlockOnBoard(x, y, block, blockId){
        //x/y indicates the top left of the tetronimo btw

        block.forEach((row, yIndex) => {
            row.forEach((element, xIndex) => {
                var finalX = x + xIndex;
                var finalY = y + yIndex;

                var isNotEmptySpace = block[yIndex][xIndex] != 0;
                var isInBounds = finalX < this.Columns && finalY < this.Rows;

                if(isNotEmptySpace && isInBounds){
                    this.Board[finalY][finalX] = blockId;
                }
            });
        });
    }

    /** Detect if a line clear has occurred. If so it drops the line, and returns a score.
        @param {number} scoreMult The current level to multiply the score by before returning.
        @returns {number} The score a user earned from this turn.
    */
    scoreLines(scoreMult){
        var rows = 0;

        this.Board.forEach((row, y) => {
            let numFilled = row.filter(val => val != 0).length;
            if(numFilled == row.length){
                this.dropLine(y);
                rows++;
            }
        });
        
        this.LinesScored += rows;

        switch(rows){
            case 1: return 100*scoreMult;
            case 2: return 300*scoreMult;
            case 3: return 500*scoreMult;
            case 4: return 800*scoreMult;
            default: return 0;
        }
    }

    /** Drops a specified row and adds a new one to the beginning.
        @param {number} rowIndex The yvalue, or the row to drop.
    */
    dropLine(rowIndex){
        this.Board.splice(rowIndex, 1);
        this.Board.unshift(new Array(this.Columns).fill(0));
    }

    /** Draws the game board (and user block) to the internally stored canvas.
        @param {number} cursorX Horizontal location of the user cursor on the board.
        @param {number} cursorY Vertical location of the user cursor on the board.
        @param {number[][]} cursorBlock A matrix representing the current user block to be drawn.
        @param {number} cursorBlockId An int value (classically 0-7) representing a tetronimo type.
      */
    drawGameBoard(cursorX, cursorY, cursorBlock, cursorBlockId) {
        this.clearBoard();

        var blockHeight = this.Height / this.Board.length;
        var blockWidth = this.Width / this.Board[0].length;
        let w = blockWidth - this.GapPx;
        let h = blockHeight - this.GapPx;

        //Draw the board.
        this.Board.forEach((row, yIndex) => {
            row.forEach((element, xIndex) => {
                let xLoc = this.X + (xIndex * blockWidth);
                let yLoc = this.Y + (yIndex * blockHeight);

                let blockColor = Tetronimo.setFaces(this.Board[yIndex][xIndex]);

                this.drawBlock(w, h, blockColor, xLoc, yLoc);
            });
        });

        //Draw the "hovering" cursor block.
        let cursorColor = Tetronimo.setFaces(cursorBlockId);

        cursorBlock.forEach((row, yIndex) => {
            row.forEach((element, xIndex) => {
                if(element != 0) { 	//Don't draw empty space!
                    let xLoc = this.X + ((cursorX + xIndex) * blockWidth);
                    let yLoc = this.Y + ((cursorY + yIndex) * blockHeight);

                    this.drawBlock(w, h, cursorColor, xLoc, yLoc);
                }
            });
        });
    }

    /** Draw an individual board block on the canvas.
     *  Theoretically this is extendable to being used for more than just colors.
     *	@param {number} width The pixel width of the block to be drawn.
     *	@param {number} height The pixel height of the block to be drawn.
     *	@param {string} color The color string of the block, usually a hexcode.
     *	@param {number} x The pixel X location to start drawing the block.
     *	@param {number} y The pixel Y location to start drawing the block.
     */
    drawBlock(width, height, color, x, y) {
        let ctx = this.Context;
        ctx.fillStyle = color;
        ctx.fillRect(x, y, width, height);
    }
}

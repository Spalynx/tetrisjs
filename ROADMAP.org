#+Title: HTML Tetris - ROADMAP

This is a simple org file with simple version requirements and todos to keep me honest.

The general mission statement is to make a sort-of functional game and abandon it, since this is more of a learner project than anything.


* Roadmap

** Basic Game (v0.1) [12/12]
*** DONE Draw simple game array.
CLOSED: [2024-01-19 Fri 00:10]
- State "DONE"       from "TODO"       [2024-01-19 Fri 00:10]
*** DONE Add tetronimos.
CLOSED: [2024-01-19 Fri 00:10]
- State "DONE"       from "TODO"       [2024-01-19 Fri 00:10]
*** DONE Draw colored blocks.
CLOSED: [2024-01-19 Fri 00:10]
- State "DONE"       from "TODO"       [2024-01-19 Fri 00:10]
*** DONE Run on ticks.
CLOSED: [2024-01-19 Fri 00:10]
- State "DONE"       from "TODO"       [2024-01-19 Fri 00:10]
*** DONE Add "hovering" user block.
CLOSED: [2024-01-19 Fri 00:10]
- State "DONE"       from "TODO"       [2024-01-19 Fri 00:10]
*** DONE User block movement.
CLOSED: [2024-01-19 Fri 00:10]
- State "DONE"       from "TODO"       [2024-01-19 Fri 00:10]
*** DONE User block rotation.
CLOSED: [2024-01-19 Fri 00:10]
- State "DONE"       from "TODO"       [2024-01-19 Fri 00:10]
*** DONE Block goes down every tick.
CLOSED: [2024-01-19 Fri 00:11]
- State "DONE"       from "TODO"       [2024-01-19 Fri 00:11]
*** DONE Spacebar to drop a block.
CLOSED: [2024-01-19 Fri 00:11]
- State "DONE"       from "TODO"       [2024-01-19 Fri 00:11]
*** DONE Block collision detection.
CLOSED: [2024-02-02 Fri 23:39]
- State "DONE"       from "TODO"       [2024-02-02 Fri 23:39]
*** DONE "Touchdown" collision.
CLOSED: [2024-02-05 Mon 23:24]
- State "DONE"       from "TODO"       [2024-02-05 Mon 23:24]
**** Essentially how you place blocks.
*** DONE Draw hovering block with gameboard.
CLOSED: [2024-02-05 Mon 23:25]
- State "DONE"       from "TODO"       [2024-02-05 Mon 23:25]
** Scoring (v0.2) [5/5]
*** DONE Keep score (bad for relationships good for games).
CLOSED: [2024-02-06 Tue 00:05]
- State "DONE"       from "TODO"       [2024-02-06 Tue 00:05]
*** DONE Gain points for placing a block.
CLOSED: [2024-02-06 Tue 00:05]
- State "DONE"       from "TODO"       [2024-02-06 Tue 00:05]
*** DONE Gain points for a tetris.
CLOSED: [2024-02-06 Tue 00:05]
- State "DONE"       from "TODO"       [2024-02-06 Tue 00:05]
*** DONE Gain more points for multiple tetris at once.
CLOSED: [2024-02-06 Tue 00:05]
- State "DONE"       from "TODO"       [2024-02-06 Tue 00:05]
*** DONE Gain points by going faster (pressing down, or pressing space);
CLOSED: [2024-02-06 Tue 00:05]
- State "DONE"       from "TODO"       [2024-02-06 Tue 00:05]
** Levels (v0.3) [6/6]
*** DONE Keep track of levels.
CLOSED: [2024-02-06 Tue 00:05]
- State "DONE"       from "TODO"       [2024-02-06 Tue 00:05]
*** DONE Keep track of tetris lines.
CLOSED: [2024-02-06 Tue 23:06]
- State "DONE"       from "TODO"       [2024-02-06 Tue 23:06]
*** DONE Iterate level based on tetris lines (probably 10 lines per level?)
CLOSED: [2024-02-06 Tue 23:06]
- State "DONE"       from "TODO"       [2024-02-06 Tue 23:06]
*** DONE Faster ticks based on level.
CLOSED: [2024-02-06 Tue 23:06]
- State "DONE"       from "TODO"       [2024-02-06 Tue 23:06]
*** DONE Add point modifier based on level.
CLOSED: [2024-02-06 Tue 23:06]
- State "DONE"       from "TODO"       [2024-02-06 Tue 23:06]
*** DONE Add game over.
CLOSED: [2024-02-06 Tue 23:06]
- State "DONE"       from "TODO"       [2024-02-06 Tue 23:06]
**** if a block is placed that touches top, cya.
** UI/UX (v0.4) [/]
*** TODO Game Over Screen
**** Simple, displays score with a try again button.
*** TODO Frame around game.
*** TODO Score, Level, Lines display.
*** TODO Next block display.

export function rotateMatrix(matrix){
    var len = matrix.length;

    //TODO: this is ripped off from the internet, I need to rewrite this when i push
    //1. transpose
    //2. flip horizontally

    for (let i = 0; i < len / 2; i++) {
        for (let j = i; j < len - i - 1; j++) {
            let temp = matrix[i][j];
            matrix[i][j] = matrix[len-1-j][i];
            matrix[len-1-j][i] = matrix[len-1-i][len-1-j];
            matrix[len-1-i][len-1-j] = matrix[j][len-1-i];
            matrix[j][len-1-i] = temp;
        }
    }
}

export function cloneMatrix(array){
    var cloned = [];
    array.forEach((column) =>
        cloned.push([...column]));
    return cloned;
}

/** Given an integer maatrix, sums all of the values within.
    @param {int[]} array A 2d array (matrix) of integers.
  */
export function SumMatrix(matrix){
    return matrix.flat()
        .reduce((partial, i) => partial + ((i == "0" || i == 0) ? 0 : 1));
}

export function PrintMatrix(matrix){
    matrix.forEach((x) => {
        var line = "";
        x.forEach((i) => {
            line += i;
        });
        console.log(line);
    });
}

export function TrimEmptyRows(matrix){
    if(matrix.length == 0) return matrix;

    var clonedMatrix = this.cloneMatrix(matrix);

    for(var i = clonedMatrix.length-1; i >= 0; i--){
        if(this.SumMatrix(clonedMatrix[i]) != 0) {
            return clonedMatrix.slice(0,i+1); //<---
        }
    }
    return clonedMatrix;
    //return clonedMatrix.filter((row) => {
        //return this.SumMatrix(row) != 0;
    //});
}
